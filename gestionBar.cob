       IDENTIFICATION DIVISION.
       PROGRAM-ID. GESTIONBAR.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT fconsommation ASSIGN TO "fconsommation.dat"
       ORGANIZATION indexed
       ACCESS IS dynamic
       RECORD KEY fconso_nom
       FILE STATUS IS fconso_stat.

       SELECT fcommande ASSIGN TO "fcommande.dat"
       ORGANIZATION sequential
       ACCESS IS sequential
       FILE STATUS IS fcommande_stat.
       
       SELECT fvente ASSIGN TO "fvente.dat"
       ORGANIZATION sequential
       ACCESS IS sequential
       FILE STATUS IS fvente_stat.

       SELECT fclient ASSIGN TO "fclient.dat"
       ORGANIZATION indexed
       ACCESS IS dynamic
       RECORD KEY fclient_nom
       FILE STATUS IS fclient_stat.

       DATA DIVISION.
       FILE SECTION.
       FD fconsommation.
       01 consoTampon.
           02 fconso_id PIC 9(15).
           02 fconso_nom PIC X(30).
           02 fconso_prixVente PIC 9(4).
           02 fconso_quantite PIC 9(6).
           
       FD fcommande.
       01 comTampon.
           02 fcom_num PIC 9(15).
           02 fcom_boisson PIC X(30).
           02 fcom_quantite PIC 9(6).
           02 fcom_date PIC 9(8).

       FD fvente.
       01 venteTampon.
           02 fvente_conso PIC X(30).
           02 fvente_quantite PIC 9(6).
           02 fvente_client PIC 9(15).
           02 fvente_date PIC 9(8).

       FD fclient.
       01 clientTampon.
           02 fclient_id PIC 9(15).
           02 fclient_nom PIC X(30).
           02 fclient_prenom PIC X(30).
           02 fclient_age PIC 9(3).

       WORKING-STORAGE SECTION.
           77 fconso_stat PIC 9(2).
           77 fcommande_stat PIC 9(2).
           77 fvente_stat PIC 9(2).
           77 fclient_stat PIC 9(2).
           77 Wtrouve PIC 9.
           77 Wid PIC 9(15).
           77 Wfin PIC 9(15).
           77 Wrep PIC 9(15).
           77 Wrole PIC 9(15).
           77 Wtache PIC 9(15).
           77 clientId PIC 9(15).

       PROCEDURE DIVISION.
       OPEN INPUT fconsommation
       IF fconso_stat=35 THEN
           OPEN OUTPUT fconsommation
       END-IF
       CLOSE fconsommation

       OPEN INPUT fcommande
       IF fcommande_stat=35 THEN
           OPEN OUTPUT fcommande
       END-IF
       CLOSE fcommande

       OPEN INPUT fvente
       IF fvente_stat=35 THEN
           OPEN OUTPUT fvente
       END-IF
       CLOSE fvente

       OPEN INPUT fclient
       IF fclient_stat=35 THEN
           OPEN OUTPUT fclient
       END-IF
       CLOSE fclient

       PERFORM MENU-BAR
       STOP RUN.

       AJOUT_CONSO.
           PERFORM WITH TEST AFTER UNTIL Wtrouve = 0
               DISPLAY "Veuillez saisir un identifiant"
               ACCEPT Wid
               PERFORM EXIST_ID
           END-PERFORM
           MOVE Wid TO fconso_id
           DISPLAY "Veuillez saisir un nom de boisson"
           ACCEPT fconso_nom
           DISPLAY "Veuillez saisir un prix de vente"
           ACCEPT fconso_prixVente
           DISPLAY "Veuillez saisir une quantite"
           ACCEPT fconso_quantite
           OPEN EXTEND fconsommation
           WRITE consoTampon END-WRITE
           CLOSE fconsommation
           PERFORM MENU-BAR  
       .
       

       EXIST_ID.
           OPEN INPUT fconsommation
           MOVE 0 TO Wfin
           MOVE 0 TO Wtrouve
           PERFORM WITH TEST AFTER UNTIL Wfin=1
               READ fconsommation
               AT END MOVE 1 TO Wfin
               NOT AT END  
                   IF fconso_id = Wid THEN
                       DISPLAY "L identiiant existe deja"
                       MOVE 1 TO Wtrouve
                   END-IF
               END-READ
           END-PERFORM
           CLOSE fconsommation
               
       .

       AFFICH_BOISSON.
           OPEN INPUT fconsommation
           MOVE 0 TO Wfin
           PERFORM WITH TEST AFTER UNTIL Wfin=1
               READ fconsommation NEXT
               AT END MOVE 1 TO Wfin
               NOT AT END
                   DISPLAY "NOM : ", fconso_nom
                   DISPLAY "PRIX DE VENTE : ", fconso_prixVente
                   DISPLAY "QUANTITE : ", fconso_quantite
               END-READ
           END-PERFORM
           CLOSE fconsommation
       .
            


       AJOUT_CLIENT.
       MOVE 0 TO Wfin
       MOVE 0 TO Wid
       PERFORM WITH TEST AFTER UNTIL Wrep = 0

        PERFORM WITH TEST AFTER UNTIL Wid = 0
          OPEN INPUT fclient
          PERFORM WITH TEST AFTER UNTIL Wfin=1
           IF Wid = 1 THEN
            MOVE 0 TO Wid
            DISPLAY "L'utilisateur existe déja"
           END-IF
           DISPLAY "Entrer l'ID de l'utilisateur"
           ACCEPT clientId
           READ fclient NEXT
           AT END MOVE 1 TO Wfin
           NOT AT END 
            IF clientId = fclient_id THEN
              MOVE 1 TO Wid
            END-IF
           END-READ
          END-PERFORM

          DISPLAY "OK vous pouvez ajouter un client avec cette id !"
          MOVE clientId TO fclient_id
          DISPLAY "Entrer le nom du client"
          ACCEPT fclient_nom
          DISPLAY "Entrer le prenom du client"
          ACCEPT fclient_prenom
          DISPLAY "Entrer l'age du client"
          ACCEPT fclient_age
          OPEN EXTEND fclient
          WRITE clientTampon END-WRITE
          CLOSE fclient
          DISPLAY "Voulez-vous entrez un autre nouveau client ? (1 = Oui / 0 = Non)"
          ACCEPT Wrep
         END-PERFORM
       END-PERFORM
       PERFORM MENU-BAR 
       .

       AFFICH_CLIENT.
       OPEN INPUT fclient
       MOVE 0 TO Wfin
       PERFORM WITH TEST AFTER UNTIL Wfin=1
           READ fclient NEXT
           AT END MOVE 1 TO Wfin
           NOT AT END
               DISPLAY "Id : ", fclient_id
               DISPLAY "NOM : ", fclient_nom
               DISPLAY "PRENOM : ", fclient_prenom
               DISPLAY "AGE : ", fclient_age
           END-READ
       END-PERFORM
       CLOSE fclient
       DISPLAY "Voilà la liste des clients"
       PERFORM MENU-BAR 
       .

       MENU-BAR.
       MOVE 0 TO Wrole
       MOVE 0 TO Wtache
       DISPLAY " --------- ACCUEIL ---------"
       DISPLAY "|                           |"
       DISPLAY "| 1 - Employé               |"
       DISPLAY "| 2 - Client                |"
       DISPLAY "|                           |"
       DISPLAY "| 3 - Quitter l'application |"
       DISPLAY " ---------------------------"
       DISPLAY "Quel est votre rôle ? "
           ACCEPT Wrole
           IF Wrole = 1 THEN
               DISPLAY " --- Bienvenue cher employé ! ---"
               DISPLAY "|                                |"
               DISPLAY "| 1 - Ajouter Boisson            |"
               DISPLAY "| 2 - Supprimer Boisson          |"
               DISPLAY "| 3 - Modifier Boisson           |"
               DISPLAY "| 4 - Ajouter Client             |"
               DISPLAY "| 5 - Historique Stock           |"
               DISPLAY "| 6 - Meilleures Ventes          |"
               DISPLAY "| 7 - Meilleurs Clients          |"
               DISPLAY "| 8 - Réapprovisionnement        |"
               DISPLAY "|                                |"
               DISPLAY "| 9 - <-- Retour                 |"
               DISPLAY " --------------------------------"
               DISPLAY "Que voulez-vous faire ?"
                   ACCEPT Wtache
                   IF Wtache = 1 THEN
                    DISPLAY "Ajouter une Boisson"
                    PERFORM AJOUT_CONSO
                   ELSE
                    IF Wtache = 2 THEN
                     DISPLAY "Supprimer une Boisson"
                    ELSE
                     IF Wtache = 3 THEN
                       DISPLAY "Modifier une Boisson"
                     ELSE
                      IF Wtache = 4 THEN
                       DISPLAY "Ajouter un Client"
                       PERFORM AJOUT_CLIENT
                      ELSE
                       IF Wtache = 5 THEN
                        DISPLAY "Historique Stock"
                        PERFORM AFFICH_CLIENT
                       ELSE
                        IF Wtache = 6 THEN
                         DISPLAY "Meilleures ventes du mois"
                        ELSE 
                         IF Wtache = 7 THEN
                          DISPLAY "Meilleurs clients du mois"
                         ELSE
                          IF Wtache = 8 THEN
                           DISPLAY "Réapprovisionnement"
                          ELSE
                           IF Wtache = 9 THEN
                            PERFORM MENU-BAR
                           ELSE
                            DISPLAY "ERREUR : Veuillez saisir un chiffre entre 1 et 9, Retour a l'accueil !"
                            PERFORM MENU-BAR
                   END-IF
           ELSE 
               IF Wrole = 2 THEN 
                   DISPLAY " --- Bienvenue cher client ! ---"
                   DISPLAY "|                               |"
                   DISPLAY "| 1 - Commander                 |"
                   DISPLAY "| 2 - Consulter la carte        |"
                   DISPLAY "|                               |"
                   DISPLAY "| 3 - <-- Retour                |"
                   DISPLAY " -------------------------------"
                   DISPLAY "Que voulez-vous faire ?"
                   ACCEPT Wtache
                   IF Wtache = 1 THEN
                    DISPLAY "Commander"
                   ELSE
                    IF Wtache = 2 THEN
                     DISPLAY "Consulter la carte"
                     PERFORM AFFICH_BOISSON
                    ELSE
                     IF Wtache = 3 THEN
                      PERFORM MENU-BAR
                     ELSE 
                      DISPLAY "ERREUR : Veuillez saisir un chiffre entre 1 et 3, Retour a l'accueil !"
                      PERFORM MENU-BAR
               ELSE 
                   IF Wrole = 3 THEN
                       EXIT 
                   ELSE
                       DISPLAY "Veuillez saisir 1, 2 ou 3 !"
                       PERFORM MENU-BAR
           END-IF
       .